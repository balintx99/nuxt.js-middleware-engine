export default class AuthEngine {
  midds = {}

  addMiddleware( page, conFun, redirect = '/', objKeys = [], argums = []) {
    this.midds[page] = {
      conFun,
      redirect,
      objKeys,
      argums
    } 
  }

  handleRouteChange( mObj ) {
    return new Promise( async (resolve, reject) => {
      let to = mObj.route.name
  
      if( !this.midds[to] ) return resolve()

      if( !await this.midds[to].conFun( ...this.midds[to].objKeys.map( k => mObj[k] ), ...this.midds[to].argums ) )
        return resolve( mObj.redirect(this.midds[to].redirect) )
      return resolve()
    })
  }
}